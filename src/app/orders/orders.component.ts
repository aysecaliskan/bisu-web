import { Component, OnInit } from '@angular/core';
import {ApiserviceService} from '../apiservice.service'
import {Router, ActivatedRoute, Params} from '@angular/router';


@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private service:ApiserviceService) { }

  ordersData:any;

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
    const subscriptionId = params['subscriptionId'];
      console.log(subscriptionId);
      if(subscriptionId==undefined)
      {
        this.service.getOrdersData().subscribe((res)=>{
          console.log(res,"res==>");
            this.ordersData = res.data;
          });
      
      }
      else
      {
        this.service.getOrdersDataBySubscriptionId(subscriptionId).subscribe((res)=>{
          console.log(res,"res==>");
            this.ordersData = res.data;
          });
      
      }
    });
   
  }

}
