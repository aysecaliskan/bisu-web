import { Component, OnInit } from '@angular/core';
import {ApiserviceService} from '../apiservice.service'
import {Router, ActivatedRoute, Params} from '@angular/router';


@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.css']
})
export class ReadComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private service:ApiserviceService) { }

  readData:any;

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
    const phoneNumber = params['phoneNumber'];
      console.log(phoneNumber);
      if(phoneNumber==undefined)
      {
        this.service.getSubscriptionData().subscribe((res)=>{
          console.log(res,"res==>");
            this.readData = res.data;
          });
      
      }
      else
      {
        this.service.getSubscriptionDataByPhoneNumber(phoneNumber).subscribe((res)=>{
          console.log(res,"res==>");
            this.readData = res.data;
          });
      
      }
    });
   
  }

}
